//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ 生成的包含文件。
// 供 mytetris.rc 使用
//
#define IDC_MYICON                      2
#define IDD_MYTETRIS_DIALOG             102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_MYTETRIS                    107
#define IDI_SMALL                       108
#define IDC_MYTETRIS                    109
#define IDR_MAINFRAME                   128
#define IDC_STATIC                      -1
#define IDM_DIFF                        106
#define ID_dif1                         111
#define ID_dif2                         112
#define ID_dif3                         113
#define ID_LAYOUT1                      114
#define ID_LAYOUT2                      115
#define ID_START                        04123
#define ID_END                          041233
// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
