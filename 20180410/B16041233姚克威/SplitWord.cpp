#include<stdio.h>
#include<string.h>
#include<stdlib.h>
char**split(char* str,int *countOfWord){
    int countOfSplit=0;
	int indexOfResult=0;
	int index=0;
	int i=0;
	//去除开头的分隔符
	for(i=0;i<strlen(str);i++){
		if(str[i]>='0'&&str[i]<='9'||str[i]>='A'&&str[i]<='z')
			break;
	}
	for(int j=i;j<strlen(str);j++){
		if(str[j]>='0'&&str[j]<='9'||str[j]>='A'&&str[j]<='z')
			continue;
	    //计算不连续分隔符个数 不包含头部的分割符
		countOfSplit++;
		while(j<strlen(str)&&!(str[j]>='0'&&str[j]<='9'||str[j]>='A'&&str[j]<='z')){
		   j++;
		}
	}
	printf("countOfSplit=%d\n",countOfSplit);
	//对result初始化	可以通过计算不连续的分隔符个数来降低result的大小
	char ** result=(char**)malloc(sizeof(char*)*(countOfSplit+1));
	for(i=0;i<countOfSplit+1;i++){
		result[i]=(char*)malloc(sizeof(char)*strlen(str));
		}
	//去除开头的分隔符
	for(i=0;i<strlen(str);i++){
		if(str[i]>='0'&&str[i]<='9'||str[i]>='A'&&str[i]<='z')
			break;
	}
	//遍历字符窜
	for(;i<strlen(str);i++){
		if(str[i]>='0'&&str[i]<='9'||str[i]>='A'&&str[i]<='z'){
			result[indexOfResult][index++]=str[i];
		}else{
		   result[indexOfResult++][index]='\0';
		   index=0;
		   while(i<strlen(str)&&!(str[i]>='0'&&str[i]<='9'||str[i]>='A'&&str[i]<='z')){
			i++;
		}
		   //循环第3步i++ 由于去空格操作，i已经加1，所以应该把当前的i加入result
		   if(i<strlen(str))
			 result[indexOfResult][index++]=str[i];
		}
	}
	*countOfWord=indexOfResult;
	return result;
}
int main(){
	char s[100]="  I like play football and C++ programming!";
	int countOfWord=0;
	char **result=split(s,&countOfWord);
	int i=0;
	for(i=0;i<countOfWord;i++){
		printf("%s\n",result[i]);
	}
 return 0;
}