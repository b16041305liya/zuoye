#include <stdio.h>
#include <string.h>
int main()
{
    int i;
    char ch[50] = "";
    int length=0;
    char temp;
    gets(ch);
    length = strlen(ch);
    for (i=0;ch[i]!='\0';i++){
        if ('A' <= ch[i] && ch[i] <= 'Z')
            ch[i] = (ch[i] +3 - 'A') % 26 + 'a';
        else if ('a' <= ch[i] && ch[i] <= 'z')
            ch[i] = (ch[i] +3 - 'a') % 26 + 'A';
    }
    for (i=0;i<length/2;i++){
        temp=ch[i];
        ch[i]=ch[length-1-i];
        ch[length-1-i]=temp;
    }
    puts(ch);
    return 0;
}
