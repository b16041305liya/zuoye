//实现strcpy函数，求src长度，申请内存空间，字符串结束符 
#include<stdio.h>
#include<stdlib.h>
char* strcpy(char* src)
{
	int length=0;
	char *temp;
	for(int i=0;src[i]!=0;i++)
	{
	    length++;	
	}
	temp=(char *)malloc(sizeof(char)*(length+1));
	for(int i=0;i<length;i++)
	    temp[i]=src[i];
	temp[length]='\0';
	return temp;
	    
}
int main()
{
	char *src1;
	char src[20]="hello world!";
	src1=strcpy(src);
	printf("%s",src1);
	return 0;
}
