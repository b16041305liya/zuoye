#include<iostream>
#include<iomanip>
using namespace std;
int main()
{
       int m=4,n=4;
       int b[m][n]={0};
       int a[m][n]={0};
       cout<<"请输入原始矩阵： "<<endl;
       for(int i=0;i<m;i++)            //初始化输入数据
              for(int j=0;j<n;j++)
                     cin>>a[i][j];
       for(int i=0;i<n;i++)              //顺时针旋转矩阵
              for(int j=0;j<n;j++)
                     b[j][n-1-i]=a[i][j];
       cout<<"顺时针旋转结果："<<endl;
       for(int i=0;i<m;i++)                       //输出结果
              for(int j=0;j<n;j++)
              {
                     cout<<setw(3)<<setiosflags(ios::right)<<b[i][j]<<" ";
                     if(j==n-1)
                            cout<<endl;
              }
       for(int i=0;i<n;i++)              //逆时针旋转矩阵
              for(int j=0;j<n;j++)
                     b[n-1-j][i]=a[i][j];
       cout<<"逆时针旋转结果："<<endl;
       for(int i=0;i<m;i++)                       //输出结果
              for(int j=0;j<n;j++)
              {
                     cout<<setw(3)<<setiosflags(ios::right)<<b[i][j]<<" ";
                     if(j==n-1)
                            cout<<endl;
              }
       return 0;
}
