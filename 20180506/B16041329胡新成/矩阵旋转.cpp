/*对给定矩阵按照0°，90°，180°，270°，360°，450°。。。方式顺时针旋转，并输出结果矩阵*/
#include<iostream>
using namespace std;
int main(){
    int m,n;//初始矩阵的行列数
    cin>>m>>n;
    int s[m][n];//初始矩阵
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            cin>>s[i][j];
        }
    }
    int a[n][m];//顺时针旋转%360=90°矩阵
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            a[i][j]=s[m-1-j][i];
        }
    }
    int b[m][n];//顺时针旋转%360=180°矩阵
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            b[i][j]=a[n-1-j][i];
        }
    }
    int c[n][m];//顺时针旋转%360=270°矩阵
    for(int i=0;i<n;i++){
        for(int j=0;j<m;j++){
            c[i][j]=b[m-1-j][i];
        }
    }
    int d[m][n];//顺时针旋转%360=360°（0°）矩阵
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            d[i][j]=c[n-1-j][i];
        }
    }
    cout<<"\n";
    for(int i=0;i<m;i++){
        for(int j=0;j<n;j++){
            cout<<s[i][j]<<"\t";
        }
        cout<<"\n";
    }
    cout<<"\n";
    int angle;//旋转角度
    cin>>angle;
    if(angle%360==90){
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                cout<<a[i][j]<<"\t";
            }
            cout<<"\n";
        }
    }
    if(angle%360==180){
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                cout<<b[i][j]<<"\t";
            }
            cout<<"\n";
        }
    }
    if(angle%360==270){
        for(int i=0;i<n;i++){
            for(int j=0;j<m;j++){
                cout<<c[i][j]<<"\t";
            }
            cout<<"\n";
        }
    }
    if(angle%360==0){
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                cout<<d[i][j]<<"\t";
            }
            cout<<"\n";
        }
    }
    return 0;
}
