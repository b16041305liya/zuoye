#include<iostream>
using namespace std;
void  findMaxAndMin(int a[9],int *max,int *min){
	*max=*min=0;
	for(int i=0;i<9;i++){
		if(a[i]>a[*max]){
			*max=i;
		}else if(a[i]<a[*min]){
			*min=i;
		}
	}
}
double ave(int a[9]){
	double sum=0;
	int max,min;
	findMaxAndMin(a,&max,&min);
	for(int i=0;i<9;i++){
		if(i!=max&&i!=min){
			sum+=a[i];
		}
	}
	return sum/7;
}
int main(){
	int a[9]={78,54,46,34,85,24,57,85,100};
	cout<<ave(a);
}
