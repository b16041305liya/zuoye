#ifndef str2_h
#define str2_h
#include<stdio.h>
#include<stdlib.h>

char * strcat(char * str1, char * str2){
	if(str1==NULL||str2==NULL) return NULL;
	char * tmp=str1;
	int num1=0,num2=0;
	while(*(tmp++)) num1++;
	tmp=str2;
	while(*(tmp++)) num2++;
	char * ret = (char *)malloc(num1+num2+1);
	int i;
	tmp=ret;
	for(i=0;i<num1;i++){
		*(tmp++)=str1[i];
	}
	// i<num1 ,copy without str1's end '\x00' 
	for(i=0;i<=num2;i++){
		*(tmp++) = str2[i];
	}
	// i<=num2 ,copy with str2's end '\x00' 
	return ret;
}
#endif