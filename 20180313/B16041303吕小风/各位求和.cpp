#include<stdio.h>
int sum(int n)
{
	int s=0;
	while(n>0)
	{
		int i=n;
		while(i>0)
		{
			s+=i%10;
			i/=10;
		}
		n--;
	}
	return s;
}
int main()
{
	int n;
	scanf("%d",&n);
	printf("%d",sum(n));
	return 0;
}